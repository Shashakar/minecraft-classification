package com.shashakar.tutorialmod;

import com.shashakar.tutorialmod.init.TutorialArmorMaterials;
import com.shashakar.tutorialmod.init.TutorialBlocks;
import com.shashakar.tutorialmod.init.TutorialEntities;
import com.shashakar.tutorialmod.init.TutorialItems;
import com.shashakar.tutorialmod.init.TutorialToolMaterials;

import com.shashakar.tutorialmod.item.*;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BlockItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.SwordItem;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
public class TutorialModRegistries {

	public static final ItemGroup TUTORIAL = new TutorialItemGroup();


	@SubscribeEvent
	public static void registerBlocks(final RegistryEvent.Register<Block> event)
	{
		event.getRegistry().registerAll
		(
				TutorialBlocks.tutorial_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(5).sound(SoundType.METAL)).setRegistryName(location("tutorial_block")),
				
				//Ores
				TutorialBlocks.tutorial_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).lightValue(5).sound(SoundType.METAL)).setRegistryName(location("tutorial_ore")),
				TutorialBlocks.tutorial_ore_nether = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 3.0f).lightValue(5).sound(SoundType.METAL)).setRegistryName(location("tutorial_ore_nether"))
			
		);
		TutorialMod.LOGGER.info("Blocks registered");
		
	}
	
	@SubscribeEvent
	public static void registerItems(final RegistryEvent.Register<Item> event)
	{
		event.getRegistry().registerAll
		(
				//Items
				TutorialItems.tutorial_item = new Item(new Item.Properties().group(TUTORIAL)).setRegistryName(location("tutorial_item")),
				
				//Tools
				TutorialItems.tutorial_axe = new AxeItem(TutorialToolMaterials.tutorial_tool_mat, -1.0f, 6.0f, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_axe"),
				TutorialItems.tutorial_hoe = new HoeItem(TutorialToolMaterials.tutorial_tool_mat2, 6.0f, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_hoe"),
				TutorialItems.tutorial_pickaxe = new PickaxeItem(TutorialToolMaterials.tutorial_tool_mat, -2, 6.0f, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_pickaxe"),
				TutorialItems.tutorial_shovel = new ShovelItem(TutorialToolMaterials.tutorial_tool_mat, -2.0f, 6.0f, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_shovel"),
				TutorialItems.tutorial_sword = new SwordItem(TutorialToolMaterials.tutorial_tool_mat, 0, 6.0f, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_sword"),
				
				//Armor
				TutorialItems.tutorial_helmet = new ArmorItem(TutorialArmorMaterials.tutorial, EquipmentSlotType.HEAD, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_helmet"),
				TutorialItems.tutorial_chestplate = new ArmorItem(TutorialArmorMaterials.tutorial, EquipmentSlotType.CHEST, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_chestplate"),
				TutorialItems.tutorial_legs = new ArmorItem(TutorialArmorMaterials.tutorial, EquipmentSlotType.LEGS, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_legs"),
				TutorialItems.tutorial_boots = new ArmorItem(TutorialArmorMaterials.tutorial, EquipmentSlotType.FEET, new Item.Properties().group(TUTORIAL)).setRegistryName("tutorial_boots"),
		
				//BlockItems
				TutorialItems.tutorial_block = new BlockItem(TutorialBlocks.tutorial_block, new Item.Properties().group(TUTORIAL)).setRegistryName(TutorialBlocks.tutorial_block.getRegistryName()),
				TutorialItems.tutorial_ore = new BlockItem(TutorialBlocks.tutorial_ore, new Item.Properties().group(TUTORIAL)).setRegistryName(TutorialBlocks.tutorial_ore.getRegistryName()),
				TutorialItems.tutorial_ore_nether = new BlockItem(TutorialBlocks.tutorial_ore_nether, new Item.Properties().group(TUTORIAL)).setRegistryName(TutorialBlocks.tutorial_ore_nether.getRegistryName()),

				//Classification Items
				TutorialItems.ability_rogue = new Ability(new Item.Properties().maxStackSize(1).group(TutorialModRegistries.TUTORIAL)),
				TutorialItems.class_item_rogue = new RogueClassItem(new Item.Properties().group(TutorialModRegistries.TUTORIAL)),
				TutorialItems.class_item_mage = new MageClassItem(new Item.Properties().group(TutorialModRegistries.TUTORIAL)),
				TutorialItems.class_item_cleric = new ClericClassItem(new Item.Properties().group(TutorialModRegistries.TUTORIAL)),
				TutorialItems.class_item_fighter = new FighterClassItem(new Item.Properties().group(TutorialModRegistries.TUTORIAL))
		);
		
		TutorialEntities.registerEntitySpawnEggs(event);
		
		TutorialMod.LOGGER.info("Items registered");
		
	}
	
	@SubscribeEvent
	public static void registerEntities(final RegistryEvent.Register<EntityType<?>> event)
	{
		event.getRegistry().registerAll
		(
				TutorialEntities.TUTORIAL_ENTITY
		);
		
		TutorialEntities.registerEntityWorldSpawns();
		
		TutorialMod.LOGGER.info("Entities have been registered");
		
	}

	
	public static ResourceLocation location(String name)
	{
		return new ResourceLocation(TutorialMod.MODID, name);
	}
}
