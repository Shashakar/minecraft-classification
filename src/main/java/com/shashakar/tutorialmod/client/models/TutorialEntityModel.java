package com.shashakar.tutorialmod.client.models;

import com.shashakar.tutorialmod.entities.TutorialEntity;

import net.minecraft.client.renderer.entity.model.CowModel;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class TutorialEntityModel extends CowModel<TutorialEntity> {

}

//public class TutorialEntityModel extends EntityModel<TutorialEntity> 
//{
//}