package com.shashakar.tutorialmod.item;

public class FighterClassItem extends ClassItem {

    public FighterClassItem(Properties properties) {
        super(properties);
    }

    @Override
    public String setClassName() {
        return "Fighter";
    }
}
