package com.shashakar.tutorialmod.item;

import com.shashakar.tutorialmod.TutorialModRegistries;
import com.shashakar.tutorialmod.capabilities.playerclass.IPlayerClass;
import com.shashakar.tutorialmod.capabilities.playerclass.PlayerClass;
import net.minecraft.entity.item.EnderPearlEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class Ability extends Item {

    public Ability(Properties properties) {
        super(properties);
        setRegistryName(TutorialModRegistries.location("ability_rogue"));
    }

    private int COOLDOWN = 200;
    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if (!player.abilities.isCreativeMode) {
            stack.shrink(1);
        }

        world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_ENDER_PEARL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
        player.getCooldownTracker().setCooldown(this, COOLDOWN);
        if (!world.isRemote) {
            String classMessage = String.format("You used an ability. Its cooldown is %d seconds.", COOLDOWN/20);
            player.sendMessage(new StringTextComponent(classMessage));
        }


        player.addStat(Stats.ITEM_USED.get(this));
        return new ActionResult(ActionResultType.SUCCESS, stack);
    }
}
