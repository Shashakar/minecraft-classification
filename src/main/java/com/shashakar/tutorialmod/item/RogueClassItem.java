package com.shashakar.tutorialmod.item;

public class RogueClassItem extends ClassItem {

    public RogueClassItem(Properties properties) {
        super(properties);
    }

    @Override
    public String setClassName() {
        return "Rogue";
    }
}
