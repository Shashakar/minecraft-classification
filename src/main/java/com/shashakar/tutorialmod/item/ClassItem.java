package com.shashakar.tutorialmod.item;

import com.shashakar.tutorialmod.TutorialModRegistries;
import com.shashakar.tutorialmod.capabilities.playerclass.IPlayerClass;
import com.shashakar.tutorialmod.capabilities.playerclass.PlayerClass;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class ClassItem extends Item {

    public String CLASSNAME;

    public ClassItem(Properties properties) {
        super(properties);
        CLASSNAME = setClassName();
        setRegistryName(TutorialModRegistries.location("class_item_" + CLASSNAME.toLowerCase()));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getHeldItem(hand);

        if (!world.isRemote) {
            IPlayerClass playerClass = PlayerClass.getFromPlayer(player);
            String oldClass = playerClass.getPlayerClass();

            if(!oldClass.equals(CLASSNAME))
            {
                if (!player.abilities.isCreativeMode) { stack.shrink(1); }
                playerClass.setPlayerClass(CLASSNAME);
                String newClass = playerClass.getPlayerClass();
                String classMessage = String.format("You were a %s, but are now a %s.", oldClass, newClass);
                player.sendMessage(new StringTextComponent(classMessage));
            }
            else
            {
                String alreadyClassMessage = String.format("You are already a %s!", oldClass);
                player.sendMessage(new StringTextComponent(alreadyClassMessage));
            }

        }

        player.addStat(Stats.ITEM_USED.get(this));
        return new ActionResult(ActionResultType.SUCCESS, stack);
    }

    public String setClassName()
    {
       return "Civilian";
    }
}
