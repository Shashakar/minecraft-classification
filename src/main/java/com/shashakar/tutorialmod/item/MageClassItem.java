package com.shashakar.tutorialmod.item;

import com.shashakar.tutorialmod.TutorialModRegistries;
import com.shashakar.tutorialmod.capabilities.playerclass.IPlayerClass;
import com.shashakar.tutorialmod.capabilities.playerclass.PlayerClass;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class MageClassItem extends ClassItem {

    public MageClassItem(Properties properties) {
        super(properties);
    }

    @Override
    public String setClassName() {
        return "Mage";
    }
}
