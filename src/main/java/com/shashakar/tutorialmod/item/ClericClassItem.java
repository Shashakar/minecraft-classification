package com.shashakar.tutorialmod.item;

public class ClericClassItem extends ClassItem {

    public ClericClassItem(Properties properties) {
        super(properties);
    }

    @Override
    public String setClassName() {
        return "Cleric";
    }
}
