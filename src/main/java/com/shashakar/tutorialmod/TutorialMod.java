package com.shashakar.tutorialmod;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.shashakar.tutorialmod.capabilities.mana.IMana;
import com.shashakar.tutorialmod.capabilities.mana.Mana;
import com.shashakar.tutorialmod.capabilities.mana.ManaStorage;
import com.shashakar.tutorialmod.capabilities.playerclass.IPlayerClass;
import com.shashakar.tutorialmod.capabilities.playerclass.PlayerClass;
import com.shashakar.tutorialmod.capabilities.playerclass.PlayerClassStorage;
import com.shashakar.tutorialmod.client.renders.TutorialRenderRegistry;
import com.shashakar.tutorialmod.config.Config;
import com.shashakar.tutorialmod.handlers.EventHandler;
import com.shashakar.tutorialmod.world.OreGeneration;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;

@Mod(TutorialMod.MODID)
public class TutorialMod {

	public static TutorialMod instance;
	public static final String MODID = "shashtutorialmod";
	public static final Logger LOGGER = LogManager.getLogger(MODID);
	
	public TutorialMod() {
		
		instance = this;
		
		final String SERVER_TOML = MODID + "-server.toml";
		final String CLIENT_TOML = MODID + "-client.toml";
		
		ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, Config.SERVER_CONFIG, SERVER_TOML);
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_CONFIG, CLIENT_TOML);
		
		
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);
		

		Config.loadConfig(Config.SERVER_CONFIG, FMLPaths.CONFIGDIR.get().resolve(SERVER_TOML).toString());
		Config.loadConfig(Config.CLIENT_CONFIG, FMLPaths.CONFIGDIR.get().resolve(CLIENT_TOML).toString());
		
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	private void setup(final FMLCommonSetupEvent event){
		OreGeneration.setupOreGeneration();
		CapabilityManager.INSTANCE.register(IMana.class, new ManaStorage(), Mana::new);
		CapabilityManager.INSTANCE.register(IPlayerClass.class, new PlayerClassStorage(), PlayerClass::new);
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		LOGGER.info("Setup method has been registered");
	}
	
	private void clientRegistries(final FMLClientSetupEvent event){
		TutorialRenderRegistry.registryEntityRenders();
		LOGGER.info("clientRegistries has been registered");
	}
}
