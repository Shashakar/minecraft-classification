package com.shashakar.tutorialmod;

import com.shashakar.tutorialmod.init.TutorialItems;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class TutorialItemGroup extends ItemGroup {

	public TutorialItemGroup() {
		super("tutorial");
		// TODO Auto-generated constructor stub
	}

	@Override
	public ItemStack createIcon() {
		// TODO Auto-generated method stub
		return new ItemStack(TutorialItems.tutorial_pickaxe);
	}

	
}
