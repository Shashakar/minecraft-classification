package com.shashakar.tutorialmod.capabilities.mana;

public class Mana implements IMana {

	private float mana;
	
	public Mana()
	{
		this.mana = 250.0f;
	}
	
	
	@Override
	public void consume(float points) {
		this.mana -= points;
		
		if (this.mana < 0.0f) this.mana = 0.0f;
	}

	@Override
	public void fill(float points) {
		this.mana += points;
		
	}

	@Override
	public void set(float points) {
		this.mana = points;
		
	}

	@Override
	public float getMana() {
		return this.mana;
	}
	
	

}
