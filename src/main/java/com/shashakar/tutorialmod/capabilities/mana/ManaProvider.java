package com.shashakar.tutorialmod.capabilities.mana;

import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

/**
* Mana provider
*
* This class is responsible for providing a capability. Other modders may
* attach their own provider with implementation that returns another
* implementation of IMana to the target's (Entity, TE, ItemStack, etc.) disposal.
*/
public class ManaProvider implements ICapabilitySerializable<INBT>
{
	@CapabilityInject(IMana.class)
	public static final Capability<IMana> MANA_CAP = null;
	
	private LazyOptional<IMana> instance = LazyOptional.of(MANA_CAP::getDefaultInstance);

	
	@Override
	public INBT serializeNBT()
	{
		return MANA_CAP.getStorage().writeNBT(MANA_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null);
	}

	@Override
	public void deserializeNBT(INBT nbt) {
		MANA_CAP.getStorage().readNBT(MANA_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null, nbt);
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction side) {
		return capability == MANA_CAP ? instance.cast() : LazyOptional.empty();
	}
	

}