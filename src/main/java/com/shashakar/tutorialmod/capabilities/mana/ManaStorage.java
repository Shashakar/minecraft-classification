package com.shashakar.tutorialmod.capabilities.mana;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class ManaStorage implements IStorage<IMana>
{
	@Override
	public INBT writeNBT(Capability<IMana> capability, IMana instance, Direction side) {
		CompoundNBT tag = new CompoundNBT();
		tag.putFloat("mana", instance.getMana());
		return tag;
	}

	@Override
	public void readNBT(Capability<IMana> capability, IMana instance, Direction side, INBT nbt) {
		CompoundNBT tag = (CompoundNBT) nbt;
		instance.set(tag.getFloat("mana"));
	}
}
