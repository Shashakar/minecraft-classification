package com.shashakar.tutorialmod.capabilities.playerclass;

import net.minecraft.entity.player.PlayerEntity;

public class PlayerClass implements IPlayerClass {

	private String playerClass;
	private int playerLevel;
	
	public PlayerClass()
	{
		this.playerClass = "None";
		this.playerLevel = 1;
	}

	public static IPlayerClass getFromPlayer(PlayerEntity player) {
		return player.getCapability(PlayerClassProvider.PLAYER_CLASS_CAP, null).orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!"));
	}

	@Override
	public void setPlayerClass(String playerClass) {
		this.playerClass = playerClass;
	}

	@Override
	public void setLevel(int playerLevel) {
		this.playerLevel = playerLevel;
	}

	@Override
	public String getPlayerClass() {
		return this.playerClass;
	}

	@Override
	public int getLevel() {
		return this.playerLevel;
	}

	

}
