package com.shashakar.tutorialmod.capabilities.playerclass;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class PlayerClassStorage implements IStorage<IPlayerClass>
{
	@Override
	public INBT writeNBT(Capability<IPlayerClass> capability, IPlayerClass instance, Direction side) {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("playerClass", instance.getPlayerClass());
		tag.putFloat("classLevel", instance.getLevel());
		return tag;
	}

	@Override
	public void readNBT(Capability<IPlayerClass> capability, IPlayerClass instance, Direction side, INBT nbt) {
		CompoundNBT tag = (CompoundNBT) nbt;
		instance.setPlayerClass(tag.getString("playerClass"));
		instance.setLevel(tag.getInt("classLevel"));
	}
}
