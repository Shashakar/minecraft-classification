package com.shashakar.tutorialmod.capabilities.playerclass;

import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

/**
* PlayerClass provider
*
* This class is responsible for providing a capability. Other modders may
* attach their own provider with implementation that returns another
* implementation of IPlayerClass to the target's (Entity, TE, ItemStack, etc.) disposal.
*/
public class PlayerClassProvider implements ICapabilitySerializable<INBT>
{
	@CapabilityInject(IPlayerClass.class)
	public static final Capability<IPlayerClass> PLAYER_CLASS_CAP = null;
	
	private LazyOptional<IPlayerClass> instance = LazyOptional.of(PLAYER_CLASS_CAP::getDefaultInstance);

	
	@Override
	public INBT serializeNBT()
	{
		return PLAYER_CLASS_CAP.getStorage().writeNBT(PLAYER_CLASS_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null);
	}

	@Override
	public void deserializeNBT(INBT nbt) {
		PLAYER_CLASS_CAP.getStorage().readNBT(PLAYER_CLASS_CAP, this.instance.orElseThrow(() -> new IllegalArgumentException("LazyOptional must not be empty!")), null, nbt);
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction side) {
		return capability == PLAYER_CLASS_CAP ? instance.cast() : LazyOptional.empty();
	}
	

}