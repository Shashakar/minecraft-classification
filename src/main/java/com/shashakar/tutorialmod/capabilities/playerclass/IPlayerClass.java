package com.shashakar.tutorialmod.capabilities.playerclass;

public interface IPlayerClass {
	void setPlayerClass(String playerClass);
	void setLevel(int level);
	
	String getPlayerClass();
	int getLevel();
}
