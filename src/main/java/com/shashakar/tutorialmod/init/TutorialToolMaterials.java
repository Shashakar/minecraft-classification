package com.shashakar.tutorialmod.init;

import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;

public enum TutorialToolMaterials implements IItemTier 
{
	
	//Tool materials here:
	tutorial_tool_mat(10.0f, 9.0f, 800, 3, 25, TutorialItems.tutorial_item),
	tutorial_tool_mat2(5.0f, 4.5f, 400, 3, 25, TutorialItems.tutorial_item);
	
	
	private float attackDamage, efficiency;
	private int durability, enchantability, harvestLevel;
	private Item repairMaterial;

	private TutorialToolMaterials(float attackDamage, float efficiency, int durability, int enchantability, int harvestLevel, Item repairMaterial) 
	{
		
		this.attackDamage = attackDamage;
		this.efficiency = efficiency;
		this.durability = durability;
		this.harvestLevel = harvestLevel;
		this.enchantability = enchantability;
		this.repairMaterial = repairMaterial;
	}
	
	@Override
	public float getAttackDamage() {
		// TODO Auto-generated method stub
		return this.attackDamage;
	}

	@Override
	public float getEfficiency() {
		// TODO Auto-generated method stub
		return this.efficiency;
	}

	@Override
	public int getEnchantability() {
		// TODO Auto-generated method stub
		return this.enchantability;
	}

	@Override
	public int getHarvestLevel() {
		// TODO Auto-generated method stub
		return this.harvestLevel;
	}

	@Override
	public int getMaxUses() {
		// TODO Auto-generated method stub
		return this.durability;
	}

	@Override
	public Ingredient getRepairMaterial() {
		// TODO Auto-generated method stub
		return Ingredient.fromItems(this.repairMaterial);
	}

}
