package com.shashakar.tutorialmod.init;

import com.shashakar.tutorialmod.item.*;
import net.minecraft.item.Item;

public class TutorialItems 
{

	//Items
	public static Item tutorial_item;
	
	public static Item tutorial_entity_egg;
	
	//Tools
	public static Item tutorial_axe;
	public static Item tutorial_hoe;
	public static Item tutorial_pickaxe;
	public static Item tutorial_shovel;
	public static Item tutorial_sword;
	
	//Armor
	public static Item tutorial_helmet;
	public static Item tutorial_chestplate;
	public static Item tutorial_legs;
	public static Item tutorial_boots;
	
	//BlockItems
	public static Item tutorial_block;
	public static Item tutorial_ore;
	public static Item tutorial_ore_nether;
	
	//Classification Items
	public static Ability ability_rogue;
	public static RogueClassItem class_item_rogue;
	public static ClericClassItem class_item_cleric;
	public static FighterClassItem class_item_fighter;
	public static MageClassItem class_item_mage;
}