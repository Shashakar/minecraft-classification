package com.shashakar.tutorialmod.init;

import com.shashakar.tutorialmod.TutorialMod;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public enum TutorialArmorMaterials implements IArmorMaterial {
	tutorial("tutorial", 400, new int[] {8, 10, 9, 7}, 25, TutorialItems.tutorial_item, "entity.donkey.angry", 0.0f);

	private String name, equipSound;
	private int durability, enchantability;
	private int[] damageReductionAmounts;
	private float toughness;
	private Item repairMaterial;
	private static final int[] maxDamageArray = new int[] {13,15,16,11};
	
	private TutorialArmorMaterials(String name, int durability, int[] damageReductionAmounts, int enchantability, Item repairMaterial, String equipSound, float toughness)
	{
		this.name = name;
		this.equipSound = equipSound;
		this.durability = durability;
		this.enchantability = enchantability;
		this.damageReductionAmounts = damageReductionAmounts;
		this.toughness = toughness;
		this.repairMaterial = repairMaterial;
	}
	
	@Override
	public int getDamageReductionAmount(EquipmentSlotType slot) {
		return this.damageReductionAmounts[slot.getIndex()];
	}

	@Override
	public int getDurability(EquipmentSlotType slot) {
		return maxDamageArray[slot.getIndex()] * this.durability;
	}

	@Override
	public int getEnchantability() {
		return this.enchantability;
	}

	@Override
	public String getName() {
		return TutorialMod.MODID + ":" + this.name;
	}

	@Override
	public Ingredient getRepairMaterial() {
		return Ingredient.fromItems(this.repairMaterial);
	}

	@Override
	public SoundEvent getSoundEvent() {
		return new SoundEvent(new ResourceLocation(equipSound));
	}

	@Override
	public float getToughness() {
		return this.toughness;
	}

}
