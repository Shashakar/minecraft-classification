package com.shashakar.tutorialmod.handlers;

import com.shashakar.tutorialmod.TutorialMod;
import com.shashakar.tutorialmod.TutorialModRegistries;
import com.shashakar.tutorialmod.capabilities.mana.IMana;
import com.shashakar.tutorialmod.capabilities.mana.ManaProvider;
import com.shashakar.tutorialmod.capabilities.playerclass.IPlayerClass;
import com.shashakar.tutorialmod.capabilities.playerclass.PlayerClassProvider;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class EventHandler {
	
	
	public static final ResourceLocation MANA_CAP_LOC = TutorialModRegistries.location("mana");
	public static final ResourceLocation PLAYER_CLASS_CAP_LOC = TutorialModRegistries.location("player_class");
	
	@SubscribeEvent
	public void attachCapability(AttachCapabilitiesEvent<Entity> event)
	{
		TutorialMod.LOGGER.info("checking if entity is a player");
		if (event.getObject() instanceof PlayerEntity)
		{
			event.addCapability(MANA_CAP_LOC, new ManaProvider());
			TutorialMod.LOGGER.info("Added mana capability to player");
			event.addCapability(PLAYER_CLASS_CAP_LOC, new PlayerClassProvider());
			TutorialMod.LOGGER.info("Added player class capability to player");
		}
	}
	
	@SubscribeEvent
	public void onPlayerLogsIn(PlayerLoggedInEvent event)
	{
		PlayerEntity player = event.getPlayer();
		TutorialMod.LOGGER.info("Got player log in.");
		IMana mana = player.getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new RuntimeException("No Mana Capability found!"));
		IPlayerClass playerClass = player.getCapability(PlayerClassProvider.PLAYER_CLASS_CAP, null).orElseThrow(() -> new RuntimeException("No Player Class Capability found!"));
		String message = String.format("Hello there, you have %d mana left.", (int) mana.getMana());
		String pcMessage = String.format("Hello! You are currently a level %d %s.", playerClass.getLevel(), playerClass.getPlayerClass());
		player.sendMessage(new StringTextComponent(message));
		player.sendMessage(new StringTextComponent(pcMessage));
		TutorialMod.LOGGER.info(message);
	
	}

	@SubscribeEvent
	public void onPlayerSleep(PlayerSleepInBedEvent event)
	{
		PlayerEntity player = event.getPlayer();
	
		if (player.world.isRemote) return;
	
		IMana mana = player.getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new RuntimeException("No Mana Capability found!"));
	
		mana.fill(50);
	
		String message = String.format("You feel refreshed after a good night's sleep. You received 50 mana. You now have %d mana left.", (int) mana.getMana());
		player.sendMessage(new StringTextComponent(message));
	}

	@SubscribeEvent
	public void onPlayerFalls(LivingFallEvent event)
	{
		Entity entity = event.getEntity();
	
		if (entity.world.isRemote || !(entity instanceof PlayerEntity) || event.getDistance() < 3) return;
	
		PlayerEntity player = (PlayerEntity) entity;
		IMana mana = player.getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new RuntimeException("No Mana Capability found!"));
	
		float points = mana.getMana();
		float cost = event.getDistance() * 2;
		
		String message = "";
	
		if (points > cost)
		{
			mana.consume(cost);
		
			message = String.format("You absorbed fall damage. It costed %d mana, you have %d mana left.", (int) cost, (int) mana.getMana());
			
		
			event.setCanceled(true);
		}
		
		else
		{
			message = "You have fallen but your mana reserves were too low to absorb the fall damage";
			event.setCanceled(false);
		}
		player.sendMessage(new StringTextComponent(message));
	}
	
	@SubscribeEvent
	public void onPlayerClone(PlayerEvent.Clone event)
	{
		PlayerEntity player = event.getPlayer();
		
		IMana mana = player.getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new RuntimeException("No Mana Capability found!"));
		IMana oldMana = event.getOriginal().getCapability(ManaProvider.MANA_CAP, null).orElseThrow(() -> new RuntimeException("No Mana Capability found!"));
		mana.set(oldMana.getMana());
		
		IPlayerClass playerClass = player.getCapability(PlayerClassProvider.PLAYER_CLASS_CAP, null).orElseThrow(() -> new RuntimeException("No player class capability found!"));
		IPlayerClass oldClass = event.getOriginal().getCapability(PlayerClassProvider.PLAYER_CLASS_CAP, null).orElseThrow(() -> new RuntimeException("No player class capability found!"));
		playerClass.setPlayerClass(oldClass.getPlayerClass());
		playerClass.setLevel(oldClass.getLevel());
		
	}
}
